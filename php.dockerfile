FROM php:5.6-apache
LABEL maintainer "Victor Buoro <victor.buoro.silva@usp.br>"
#COPY ./php.ini /usr/local/etc/php/
RUN apt-get update && apt-get install -y \
		zlib1g-dev \
		libmcrypt-dev
RUN rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install \
		-j$(nproc) \
		iconv \
		pdo \
		mysql \
		mysqli \
		pdo_mysql \
		mbstring \
		mcrypt \
		zip

#PHP 5.6
RUN pecl install xdebug-2.5.5
RUN pecl install apcu-4.0.11

#PHP 7.0
#RUN pecl install xdebug-stable
#RUN pecl install apcu-stable

#PHP 5.6
RUN echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20131226/xdebug.so" > /usr/local/etc/php/conf.d/xdebug.ini && \

#PHP 7.0
#RUN echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20151012/xdebug.so" > /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.default_enable = 1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_enable = 1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_handler = dbgp" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_autostart = 1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_connect_back = 1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_port = 9000" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.remote_host = 192.168.0.22" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.profiler_enable=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.profiler_enable_trigger=1" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.profiler_output_dir=\"/tmp\"" >> /usr/local/etc/php/conf.d/xdebug.ini

#PHP 5.6 20131226
RUN echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20151012/opcache.so" > /usr/local/etc/php/conf.d/opcache.ini

#PHP 7.0 20151012
#RUN echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20151012/opcache.so" > /usr/local/etc/php/conf.d/opcache.ini

RUN echo "extension=apcu.so" > /usr/local/etc/php/conf.d/apcu.ini && \
    echo "apc.enable_cli=1" >> /usr/local/etc/php/conf.d/apcu.ini

RUN echo "realpath_cache_size=4096k" > /usr/local/etc/php/conf.d/tuning.ini && \
    echo "realpath_cache_ttl=300" >> /usr/local/etc/php/conf.d/tuning.ini

RUN echo "date.timezone = \"UTC\"" >> /usr/local/etc/php/conf.d/timezone.ini

RUN a2enmod rewrite

EXPOSE 80
