Instalação do Docker e Docker-compose. Links detalhados na descrição do projeto.

- Antes de mais nada, remova possíveis versões antigas do Docker:

sudo apt-get remove docker docker-engine docker.io

- Depois, atualize o banco de dados de pacotes:

sudo apt-get update

- Agora, adicione ao sistema a chave GPG oficial do repositório do Docker:

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

- Adicione o repositório do Docker às fontes do APT:

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

- Atualize o banco de dados de pacotes, pare ter acesso aos pacotes do Docker a partir do novo repositório adicionado:

sudo apt-get update

- Por fim, instale o pacote docker-ce:

sudo apt-get install docker-ce

- Caso você queira, você pode verificar se o Docker foi instalado corretamente verificando a sua versão:

sudo docker version

- E para executar o Docker sem precisar de sudo, adicione o seu usuário ao grupo docker:

sudo usermod -aG docker $(whoami)



- O Docker Compose não é instalado por padrão no Linux, então você deve instalá-lo por fora. Para tal, baixe-o na sua versão mais atual, que pode ser visualizada no seu GitHub, executando o comando abaixo:

sudo curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

- Após isso, dê permissão de execução para o docker-compose:

sudo chmod +x /usr/local/bin/docker-compose
